--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: conversions; Type: TABLE; Schema: public; Owner: deployer
--

CREATE TABLE public.conversions (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    file_id uuid NOT NULL,
    file_name text NOT NULL,
    old_format text NOT NULL,
    new_format text NOT NULL,
    created_at text NOT NULL
);


ALTER TABLE public.conversions OWNER TO deployer;

--
-- Name: formats; Type: TABLE; Schema: public; Owner: deployer
--

CREATE TABLE public.formats (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    name text NOT NULL,
    type text NOT NULL
);


ALTER TABLE public.formats OWNER TO deployer;

--
-- Data for Name: conversions; Type: TABLE DATA; Schema: public; Owner: deployer
--

COPY public.conversions (id, file_id, file_name, old_format, new_format, created_at) FROM stdin;
\.


--
-- Data for Name: formats; Type: TABLE DATA; Schema: public; Owner: deployer
--

COPY public.formats (id, name, type) FROM stdin;
6c7fe122-3e3b-11ea-8d2b-bb63141ca3f2	PNG	image/png
6c7ff9b4-3e3b-11ea-8d2b-937e81e123fb	BMP	image/bmp
6c80090e-3e3b-11ea-8d2b-cf6c5a1aa296	JPG	image/jpeg
\.


--
-- Name: conversions conversions_pkey; Type: CONSTRAINT; Schema: public; Owner: deployer
--

ALTER TABLE ONLY public.conversions
    ADD CONSTRAINT conversions_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

